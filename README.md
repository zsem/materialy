# Jak pobrać materiały
Aby pobrać materiały wystarczy, że sklonujesz repozytorium. Aby to zrobić użyj polecenia

```
git clone <adres_repozytorium>
```

Swój adres do repozytorium znajdziesz w prawym górnym rogu

![enter image description here](http://i.imgur.com/Mj8hj5p.png)

Git sklonuje repozytorium zdalne i zainicjuje lokalne repozytorium na Twoim komputerze. Aby móc w łatwy sposób aktualizować materiały wykorzystaj polecenie

```
git pull
```